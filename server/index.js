import express from 'express';
import bodyParser from 'body-parser'
import mongoose from 'mongoose';
import cors from 'cors'

import postRouter from './routes/posts.js'

const app = express()
app.use(cors())

app.use(bodyParser.json({ limit: "30mb", extended: true}))
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true}))
app.use('/posts', postRouter)

const CONNECTION_URL='mongodb+srv://m3rn4dm1n:m3rnpass911@m3rn5tack.hgwhm.gcp.mongodb.net/mernsite?retryWrites=true&w=majority'
const PORT = process.env.PORT || 5000

mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true,})
.then(() => app.listen(PORT, () => console.log(`Server listening on ${PORT}`)))
.catch((error) => console.warn(error.message))

mongoose.set('strictQuery', false)