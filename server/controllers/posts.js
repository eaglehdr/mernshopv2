import Post from '../models/posts.js'

export const getPosts = async (req,res) => {
    try {
        const postMessages = await Post.find({})
        console.log(postMessages)
        
        res.status(200).json(postMessages)
    } catch (error) {
        res.status(404).json({message: error.message})
    }
}

export const createPost = async (req,res) => {
    const post = req.body;

    const newPost = await new Post(post)
    try {
        await newPost.save();
        // console.log(post,req)
        res.status(201).json(newPost);

    } catch (error) {
        res.status(409).json({ message: error.message})        
    }
}