import { combineReducers } from 'redux'
import posts from './posts'

export default combineReducers({
    posts, // or we can do posts: posts
})